const loadingElement = document.getElementById("loader");
const letters = document.querySelectorAll(".user-character");
const buttonElement = document.getElementById('button');
const numberOfRounds = 6;
let isLoading = true
const totalChar = 5 ;

async function clickTOGetWord(){ 

 // async function init(){

  let currentRow = 0
  let guessWord = ''
  setLoader(isLoading);

  const url = 'https://words.dev-apis.com/word-of-the-day';

  const response = await fetch(url);
  const result = await response.json();
  const gameWord = result.word
  const word = gameWord.toUpperCase();
  const wordParts = word.split("");
  console.log(word);
  console.log(wordParts);
  
  isLoading = false;
  setLoader(isLoading);

  function combineLetters(letter){
    if (guessWord.length < totalChar){     
      guessWord += letter    
       }
      
    const index = currentRow * totalChar + guessWord.length - 1
    letters[index].textContent = letter
    
      }

  function removeCharacters(){
    guessWord = guessWord.substring(0, guessWord.length - 1);
    letters[currentRow * totalChar + guessWord.length].textContent = "";
    
  }
 
 async function checkForResult(){

    if (guessWord.length !== totalChar) {
      return;
    }

    const guessParts = guessWord.split("");
    let allCorrect = true;

    for (let i = 0; i < totalChar; i++) {
      if (guessParts[i] === wordParts[i]) {
        letters[currentRow * totalChar + i].classList.add("correct");
       }
    }

    for (let i = 0; i < totalChar; i++) {
      if (guessParts[i] === wordParts[i]) {
      }
      else {
        allCorrect = false;
        letters[currentRow * totalChar + i].classList.add("wrong");
      }
    } 

    currentRow++;
    guessWord = "";
    if (allCorrect) {
      alert("Win Congrats! Please refresh the page to play again");
    }
     else if (currentRow === numberOfRounds) {
      alert(`You lost, the word is ${word}. Please try again!`);
    }
    else if(currentRow < numberOfRounds){
      alert('Entered wrong. Please try again you have more chances!')      
    }
   }

  document.addEventListener("keydown",function  (event){

    event.preventDefault()
    let action = event.key
    console.log(action)
    if (action === 'Enter'){

      checkForResult();

    }else if (action === 'Backspace'){
      removeCharacters();

    }
    else   if (isLetter(action)){
      
      action = action.toUpperCase()

      combineLetters(action);
    }
  })

 function setLoader(isLoading) {
      loadingElement.classList.toggle("loader",isLoading);
  }

  function isLetter(letter) {
    return /^[a-zA-Z]$/.test(letter);
  }
};
// init();
