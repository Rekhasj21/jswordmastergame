# Word Master Game

You and I are going to recreate the well known word game Wordle. If you're not familiar with Wordle (which we'll call Word Masters), here's how you play

- There is a secret five letter word chosen
- Players have to guesses to figure out the secret word.
- If the player guesses an incorrect word, it is shown as red
- If the player guesses the right word, the player wins and the game is over.

You need to call an API to get the secret word. Frontend Masters has created one that you can call here:

**The API**

You have two APIs to work with today:

**GET https://words.dev-apis.com/word-of-the-day**

- This will give you the word of the day. It changes every night at midnight
- The response will look like this: {"word":"humph","puzzleNumber":3} where the word is the current word of the day and the puzzleNumber is which puzzle of the day it is
- If you add random=1 to the end of your URL (words.dev-apis.com/wordof-the-day/get-word-of-the-day?random=1) then it will give you a random word of the day, not just the same daily one.
- If you add puzzle=<number> to the end of your URL (words.dev-apis.com/wordof-the-day/get-word-of-the-day?puzzle=1337) then it will give you the same word every time.
